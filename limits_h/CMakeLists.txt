cmake_minimum_required(VERSION 3.5)

project(limits_h LANGUAGES C)

add_executable(limits_h main.c)

install(TARGETS limits_h
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
