cmake_minimum_required(VERSION 3.5)

project(stddef_h LANGUAGES C)

add_executable(stddef_h main.c)

install(TARGETS stddef_h
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
