#include <stdio.h>
#include <string.h>
#include<unistd.h>
// #include<windows.h>

/*
int main () {
    FILE *fp;

    fp = fopen("file.txt", "w");

    fprintf(fp, "%s", "This is tutorialspoint.com");
    fclose(fp);

    return(0);
}
*/

/*
int main () {
    FILE *fp;
    char c;

    fp = fopen("file.txt", "w");

    c = fgetc(fp);
    if( ferror(fp) ) {
        printf("Error in reading from file : file.txt\n");
    }
    clearerr(fp);

    if( ferror(fp) ) {
        printf("Error in reading from file : file.txt\n");
    }
    fclose(fp);

    return(0);
}
*/

/*
int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    if(fp == NULL) {
        perror("Error in opening file");
        return(-1);
    }

    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", c);
    }
    fclose(fp);

    return(0);
}
*/

/*
int main () {
    FILE *fp;
    char c;

    fp = fopen("file.txt", "w");

    c = fgetc(fp);
    if( ferror(fp) ) {
        printf("Error in reading from file : file.txt\n");
    }
    clearerr(fp);

    if( ferror(fp) ) {
        printf("Error in reading from file : file.txt\n");
    }
    fclose(fp);

    return(0);
}
*/

/*
int main () {

    char buff[1024];

    memset( buff, '\0', sizeof( buff ));

    fprintf(stdout, "Going to set full buffering on\n");
    setvbuf(stdout, buff, _IOFBF, 1024);

    fprintf(stdout, "This is tutorialspoint.com\n");
    fprintf(stdout, "This output will go into buff\n");
    fflush( stdout );

    fprintf(stdout, "and this will appear when programm\n");
    fprintf(stdout, "will come after sleeping 5 seconds\n");

    sleep(5);

    return(0);
}
*/

/*
int main () {
   FILE *fp;
   fpos_t position;

   fp = fopen("file.txt","w+");
   fgetpos(fp, &position);
   fputs("Hello, World!", fp);

   fsetpos(fp, &position);
   fputs("This is going to override previous content", fp);
   fclose(fp);

   return(0);
}

int main () {
   FILE *fp;
   int c;
   int n = 0;

   fp = fopen("file.txt","r");

   while(1) {
       c = fgetc(fp);
       if( feof(fp) ) {
           break ;
       }
       printf("%c", c);
   }

   fclose(fp);

   return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    FILE * fp;

    fp = fopen ("file.txt", "w+");
    fprintf(fp, "%s %s %s %d", "We", "are", "in", 2012);

    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", c);
    }
    fclose(fp);

    return(0);
}
*/

/*
int main () {
    FILE *fp;
    char c[] = "this is tutorialspoint";
    char buffer[100];*/

    /* Open file for both reading and writing */
    /*fp = fopen("file.txt", "w+");*/

    /* Write data to the file */
    /*fwrite(c, strlen(c) + 1, 1, fp);*/

    /* Seek to the beginning of the file */
    /*fseek(fp, 0, SEEK_SET);*/

    /* Read and display data */
    /*fread(buffer, strlen(c)+1, 1, fp);
    printf("%s\n", buffer);
    fclose(fp);

    return(0);
}
*/

/*
int main () {
    FILE *fp;

    printf("This text is redirected to stdout\n");

    fp = freopen("file.txt", "w+", stdout);

    printf("This text is redirected to file.txt\n");

    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
int main () {
    FILE *fp;

    fp = fopen("file.txt","w+");
    fputs("This is tutorialspoint.com", fp);

    fseek( fp, 7, SEEK_SET );
    fputs(" C Programming Language", fp);
    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
int main () {
    FILE *fp;
    fpos_t position;

    fp = fopen("file.txt","w+");
    fgetpos(fp, &position);
    fputs("Hello, World!", fp);

    fsetpos(fp, &position);
    fputs("This is going to override previous content", fp);
    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
int main () {
   FILE *fp;
   int len;

   fp = fopen("file.txt", "r");
   if( fp == NULL )  {
      perror ("Error opening file");
      return(-1);
   }
   fseek(fp, 0, SEEK_END);

   len = ftell(fp);
   fclose(fp);

   printf("Total size of file.txt = %d bytes\n", len);

   return(0);
}
*/

/*
int main () {
    FILE *fp;
    char str[] = "This is tutorialspoint.com";

    fp = fopen( "file.txt" , "w" );
    fwrite(str , 1 , sizeof(str) , fp );

    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
#include <stdio.h>
#include <string.h>

int main () {
    int ret;
    FILE *fp;
    char filename[] = "file.txt";

    fp = fopen(filename, "w");

    fprintf(fp, "%s", "This is tutorialspoint.com");
    fclose(fp);

    ret = remove(filename);

    if(ret == 0) {
        printf("File deleted successfully");
    } else {
        printf("Error: unable to delete the file");
    }

    return(0);
}
*/

/*
int main () {
    int ret;
    char oldname[] = "file.txt";
    char newname[] = "newfile.txt";

    ret = rename(oldname, newname);

    if(ret == 0) {
        printf("File renamed successfully");
    } else {
        printf("Error: unable to rename the file");
    }

    return(0);
}
*/

/*
int main () {
    char str[] = "This is tutorialspoint.com";
    FILE *fp;
    int ch;*/

    /* First let's write some content in the file */
    /*fp = fopen( "file.txt" , "w" );
    fwrite(str , 1 , sizeof(str) , fp );
    fclose(fp);

    fp = fopen( "file.txt" , "r" );
    while(1) {
        ch = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", ch);
    }
    rewind(fp);
    printf("\n");
    while(1) {
        ch = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", ch);

    }
    fclose(fp);

    return(0);
}
*/

/*
int main () {
    char buf[BUFSIZ];

    setbuf(stdout, buf);
    puts("This is tutorialspoint");

    fflush(stdout);
    return(0);
}
*/

/*
int main () {
    char buff[1024];

    memset( buff, '\0', sizeof( buff ));

    fprintf(stdout, "Going to set full buffering on\n");
    setvbuf(stdout, buff, _IOFBF, 1024);

    fprintf(stdout, "This is tutorialspoint.com\n");
    fprintf(stdout, "This output will go into buff\n");
    fflush( stdout );

    fprintf(stdout, "and this will appear when programm\n");
    fprintf(stdout, "will come after sleeping 5 seconds\n");

    sleep(5);

    return(0);
}
*/

/*
int main () {
    FILE *fp;

    fp = tmpfile();
    printf("Temporary file created\n");*/

    /* you can use tmp file here */

    /*fclose(fp);

    return(0);
}
*/

/*
int main () {
    char buffer[L_tmpnam];
    char *ptr;

    tmpnam(buffer);
    printf("Temporary name 1: %s\n", buffer);

    ptr = tmpnam(NULL);
    printf("Temporary name 2: %s\n", ptr);

    return(0);
}
*/

/*
int main () {
    FILE * fp;

    fp = fopen ("file.txt", "w+");
    fprintf(fp, "%s %s %s %d", "We", "are", "in", 2012);

    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
int main () {
    int ch;

    for( ch = 75 ; ch <= 100; ch++ ) {
        printf("ASCII value = %d, Character = %c\n", ch , ch );
    }

    return(0);
}
*/

/*
#include <stdio.h>
#include <math.h>

int main () {
    char str[80];

    sprintf(str, "Value of Pi = %f", M_PI);
    puts(str);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdarg.h>

void WriteFrmtd(FILE *stream, char *format, ...) {
    va_list args;

    va_start(args, format);
    vfprintf(stream, format, args);
    va_end(args);
}

int main () {
    FILE *fp;

    fp = fopen("file.txt","w");

    WriteFrmtd(fp, "This is just one argument %d \n", 10);

    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
#include <stdio.h>
#include <stdarg.h>

char buffer[80];
int vspfunc(char *format, ...) {
    va_list aptr;
    int ret;

    va_start(aptr, format);
    ret = vsprintf(buffer, format, aptr);
    va_end(aptr);

    return(ret);
}

int main () {
    int i = 5;
    float f = 27.0;
    char str[50] = "tutoriaspoint.com";

    vspfunc("%d %f %s", i, f, str);
    printf("%s\n", buffer);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>


int main () {
    char str1[10], str2[10], str3[10];
    int year;
    FILE * fp;

    fp = fopen ("file.txt", "w+");
    fputs("We are in 2012", fp);

    rewind(fp);
    fscanf(fp, "%s %s %s %d", str1, str2, str3, &year);

    printf("Read String1 |%s|\n", str1 );
    printf("Read String2 |%s|\n", str2 );
    printf("Read String3 |%s|\n", str3 );
    printf("Read Integer |%d|\n", year );

    fclose(fp);

    return(0);
}
*/

/*
int main () {
    char str1[20], str2[30];

    printf("Enter name: ");
    scanf("%19s", str1);

    printf("Enter your website name: ");
    scanf("%29s", str2);

    printf("Entered Name: %s\n", str1);
    printf("Entered Website:%s", str2);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
    int day, year;
    char weekday[20], month[20], dtm[100];

    strcpy( dtm, "Saturday March 25 1989" );
    sscanf( dtm, "%s %s %d  %d", weekday, month, &day, &year );

    printf("%s %d, %d = %s\n", month, day, year, weekday );

    return(0);
}
*/

/*
#include <stdio.h>

int main () {
    FILE *fp;
    int c;
    int n = 0;

    fp = fopen("file.txt","r");
    if(fp == NULL) {
        perror("Error in opening file");
        return(-1);
    } do {
        c = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", c);
    } while(1);

    fclose(fp);
    return(0);
}
*/

/*
int main () {
    FILE *fp;
    char str[60];*/

    /* opening file for reading */
    /*fp = fopen("file.txt" , "r");
    if(fp == NULL) {
        perror("Error opening file");
        return(-1);
    }
    if( fgets (str, 60, fp)!=NULL ) {*/
        /* writing content to stdout */
        /*puts(str);
    }
    fclose(fp);

    return(0);
}
*/

/*
int main () {
    FILE *fp;
    int ch;

    fp = fopen("file.txt", "w+");
    for( ch = 33 ; ch <= 100; ch++ ) {
        fputc(ch, fp);
    }
    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
int main () {
    FILE *fp;

    fp = fopen("file.txt", "w+");

    fputs("This is c programming.", fp);
    fputs("This is a system programming language.", fp);

    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
int main () {
    char c;

    printf("Enter character: ");
    c = getc(stdin);
    printf("Character entered: ");
    putc(c, stdout);

    return(0);
}
*/

/*
int main () {
    char c;

    printf("Enter character: ");
    c = getchar();

    printf("Character entered: ");
    putchar(c);

    return(0);
}
*/

/*
int main () {
    char str[50];

    printf("Enter a string : ");
    gets(str);

    printf("You entered: %s", str);

    return(0);
}
*/

/*
int main () {
    FILE *fp;
    int ch;

    fp = fopen("file.txt", "w");
    for( ch = 33 ; ch <= 100; ch++ ) {
        putc(ch, fp);
    }
    fclose(fp);

    return(0);
}

int main () {
    FILE *fp;
    int c;

    fp = fopen("file.txt","r");
    while(1) {
        c = fgetc(fp);
        if( feof(fp) ) {
            break ;
        }
        printf("%c", c);
    }
    fclose(fp);
    return(0);
}
*/

/*
int main () {
    char ch;

    for(ch = 'A' ; ch <= 'Z' ; ch++) {
        putchar(ch);
    }

    return(0);
}
*/

/*
#include <stdio.h>
#include <string.h>

int main () {
    char str1[15];
    char str2[15];

    strcpy(str1, "tutorialspoint");
    strcpy(str2, "compileonline");

    puts(str1);
    puts(str2);

    return(0);
}
*/

/*
int main () {
    FILE *fp;
    int c;
    char buffer [256];

    fp = fopen("file.txt", "r");
    if( fp == NULL ) {
        perror("Error in opening file");
        return(-1);
    }
    while(!feof(fp)) {
        c = getc (fp);*/
        /* replace ! with + */
        /*if( c == '!' ) {
            ungetc ('+', fp);
        } else {
            ungetc(c, fp);
        }
        fgets(buffer, 255, fp);
        fputs(buffer, stdout);
    }
    return(0);
}
*/

#include <stdio.h>

int main () {
    FILE *fp;

    /* first rename if there is any file */
    rename("file.txt", "newfile.txt");

    /* now let's try to open same file */
    fp = fopen("file.txt", "r");
    if( fp == NULL ) {
        perror("Error: ");
        return(-1);
    }
    fclose(fp);

    return(0);
}
