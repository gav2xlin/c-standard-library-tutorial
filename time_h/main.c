#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/*
int main () {
    struct tm t;

    t.tm_sec    = 10;
    t.tm_min    = 10;
    t.tm_hour   = 6;
    t.tm_mday   = 25;
    t.tm_mon    = 2;
    t.tm_year   = 89;
    t.tm_wday   = 6;

    puts(asctime(&t));

    return(0);
}
*/
/*
int main () {
    clock_t start_t, end_t;
    double total_t;
    int i;

    start_t = clock();
    printf("Starting of the program, start_t = %ld\n", start_t);

    printf("Going to scan a big loop, start_t = %ld\n", start_t);
    for(i=0; i< 10000000; i++) {
    }
    end_t = clock();
    printf("End of the big loop, end_t = %ld\n", end_t);

    total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
    printf("Total time taken by CPU: %f\n", total_t  );
    printf("Exiting of the program...\n");

    return(0);
}
*/
/*
int main () {
    time_t curtime;

    time(&curtime);

    printf("Current time = %s", ctime(&curtime));

    return(0);
}
*/
/*
int main () {
    time_t start_t, end_t;
    double diff_t;

    printf("Starting of the program...\n");
    time(&start_t);

    printf("Sleeping for 5 seconds...\n");
    sleep(5);

    time(&end_t);
    diff_t = difftime(end_t, start_t);

    printf("Execution time = %f\n", diff_t);
    printf("Exiting of the program...\n");

    return(0);
}
*/
/*
#define BST (+1)
#define CCT (+8)

int main () {

    time_t rawtime;
    struct tm *info;

    time(&rawtime);*/
    /* Get GMT time */
    /*info = gmtime(&rawtime );

    printf("Current world clock:\n");
    printf("London : %2d:%02d\n", (info->tm_hour+BST)%24, info->tm_min);
    printf("China  : %2d:%02d\n", (info->tm_hour+CCT)%24, info->tm_min);

    return(0);
}
*/
/*
int main () {
    time_t rawtime;
    struct tm *info;
    time( &rawtime );
    info = localtime( &rawtime );
    printf("Current local time and date: %s", asctime(info));
    return(0);
}
*/

/*
int main () {
    int ret;
    struct tm info;
    char buffer[80];

    info.tm_year = 2001 - 1900;
    info.tm_mon = 7 - 1;
    info.tm_mday = 4;
    info.tm_hour = 0;
    info.tm_min = 0;
    info.tm_sec = 1;
    info.tm_isdst = -1;

    ret = mktime(&info);
    if( ret == -1 ) {
        printf("Error: unable to make time using mktime\n");
    } else {
        strftime(buffer, sizeof(buffer), "%c", &info );
        printf(buffer);
    }

    return(0);
}
*/

/*
int main () {
    time_t rawtime;
    struct tm *info;
    char buffer[80];

    time( &rawtime );

    info = localtime( &rawtime );

    strftime(buffer,80,"%x - %I:%M%p", info);
    printf("Formatted date & time : |%s|\n", buffer );

    return(0);
}
*/

int main () {
    time_t seconds;

    seconds = time(NULL);
    printf("Hours since January 1, 1970 = %ld\n", seconds/3600);

    return(0);
}
