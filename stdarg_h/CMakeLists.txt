cmake_minimum_required(VERSION 3.5)

project(stdarg_h LANGUAGES C)

add_executable(stdarg_h main.c)

install(TARGETS stdarg_h
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
