#include<stdarg.h>
#include<stdio.h>

int sum(int, ...);
int mul(int num_args, ...);

int main(void) {
    printf("Sum of 10, 20 and 30 = %d\n",  sum(3, 10, 20, 30) );
    printf("Sum of 4, 20, 25 and 30 = %d\n",  sum(4, 4, 20, 25, 30) );
    printf("Sum of 15 and 56 = %d\n",  sum(2, 15, 56) );
    printf("15 * 12 = %d\n",  mul(2, 15, 12) );

    return 0;
}

int sum(int num_args, ...) {
    int val = 0;
    va_list ap;
    int i;

    va_start(ap, num_args);
    for(i = 0; i < num_args; i++) {
        val += va_arg(ap, int);
    }
    va_end(ap);

    return val;
}

int mul(int num_args, ...) {
    int val = 1;
    va_list ap;
    int i;

    va_start(ap, num_args);
    for(i = 0; i < num_args; i++) {
        val *= va_arg(ap, int);
    }
    va_end(ap);

    return val;
}
