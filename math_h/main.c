#include <stdio.h>
#include <math.h>

#define PI 3.14159265

int main () {
    {
        double x, ret, val;

        x = 0.9;
        val = 180.0 / PI;

        ret = acos(x) * val;
        printf("The arc cosine of %lf is %lf degrees", x, ret);
    }

    {
        double x, ret, val;
        x = 0.9;
        val = 180.0 / PI;

        ret = asin(x) * val;
        printf("The arc sine of %lf is %lf degrees", x, ret);
    }

    {
        double x, ret, val;
        x = 1.0;
        val = 180.0 / PI;

        ret = atan (x) * val;
        printf("The arc tangent of %lf is %lf degrees", x, ret);
    }

    {
        double x, y, ret, val;

        x = -7.0;
        y = 7.0;
        val = 180.0 / PI;

        ret = atan2 (y,x) * val;
        printf("The arc tangent of x = %lf, y = %lf ", x, y);
        printf("is %lf degrees\n", ret);
    }

    {
        double x, ret, val;

        x = 60.0;
        val = PI / 180.0;
        ret = cos( x*val );
        printf("The cosine of %lf is %lf degrees\n", x, ret);

        x = 90.0;
        val = PI / 180.0;
        ret = cos( x*val );
        printf("The cosine of %lf is %lf degrees\n", x, ret);
    }

    {
        double x;

        x = 0.5;
        printf("The hyperbolic cosine of %lf is %lf\n", x, cosh(x));

        x = 1.0;
        printf("The hyperbolic cosine of %lf is %lf\n", x, cosh(x));

        x = 1.5;
        printf("The hyperbolic cosine of %lf is %lf\n", x, cosh(x));
    }

    {
        double x, ret, val;

        x = 45.0;
        val = PI / 180;
        ret = sin(x*val);
        printf("The sine of %lf is %lf degrees", x, ret);
    }

    {
        double x, ret;
        x = 0.5;

        ret = sinh(x);
        printf("The hyperbolic sine of %lf is %lf degrees", x, ret);
    }

    {
        double x, ret;
        x = 0.5;

        ret = tanh(x);
        printf("The hyperbolic tangent of %lf is %lf degrees", x, ret);
    }

    {
        double x = 0;

        printf("The exponential value of %lf is %lf\n", x, exp(x));
        printf("The exponential value of %lf is %lf\n", x+1, exp(x+1));
        printf("The exponential value of %lf is %lf\n", x+2, exp(x+2));
    }

    {
        double x = 1024, fraction;
        int e;

        fraction = frexp(x, &e);
        printf("x = %.2lf = %.2lf * 2^%d\n", x, fraction, e);
    }

    {
        double x, ret;
        int n;

        x = 0.65;
        n = 3;
        ret = ldexp(x ,n);
        printf("%f * 2^%d = %f\n", x, n, ret);
    }

    {
        double x, ret;
        x = 2.7;

        /* finding log(2.7) */
        ret = log(x);
        printf("log(%lf) = %lf", x, ret);
    }

    {
        double x, ret;
        x = 10000;

        /* finding value of log1010000 */
        ret = log10(x);
        printf("log10(%lf) = %lf\n", x, ret);
    }

    {
        double x, fractpart, intpart;

        x = 8.123456;
        fractpart = modf(x, &intpart);

        printf("Integral part = %lf\n", intpart);
        printf("Fraction Part = %lf \n", fractpart);
    }

    {
        printf("Value 8.0 ^ 3 = %lf\n", pow(8.0, 3));

        printf("Value 3.05 ^ 1.98 = %lf", pow(3.05, 1.98));
    }

    {
        printf("Square root of %lf is %lf\n", 4.0, sqrt(4.0) );
        printf("Square root of %lf is %lf\n", 5.0, sqrt(5.0) );
    }

    {
        float val1, val2, val3, val4;

        val1 = 1.6;
        val2 = 1.2;
        val3 = 2.8;
        val4 = 2.3;

        printf ("value1 = %.1lf\n", ceil(val1));
        printf ("value2 = %.1lf\n", ceil(val2));
        printf ("value3 = %.1lf\n", ceil(val3));
        printf ("value4 = %.1lf\n", ceil(val4));
    }

    {
        int a, b;
        a = 1234;
        b = -344;

        printf("The absolute value of %d is %lf\n", a, fabs(a));
        printf("The absolute value of %d is %lf\n", b, fabs(b));
    }

    {
        float val1, val2, val3, val4;

        val1 = 1.6;
        val2 = 1.2;
        val3 = 2.8;
        val4 = 2.3;

        printf("Value1 = %.1lf\n", floor(val1));
        printf("Value2 = %.1lf\n", floor(val2));
        printf("Value3 = %.1lf\n", floor(val3));
        printf("Value4 = %.1lf\n", floor(val4));
    }

    {
        float a, b;
        int c;
        a = 9.2;
        b = 3.7;
        c = 2;
        printf("Remainder of %f / %d is %lf\n", a, c, fmod(a,c));
        printf("Remainder of %f / %f is %lf\n", a, b, fmod(a,b));
    }

    return(0);
}
