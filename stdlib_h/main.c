/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
    float val;
    char str[20];

    strcpy(str, "98993489");
    val = atof(str);
    printf("String value = %s, Float value = %f\n", str, val);

    strcpy(str, "tutorialspoint.com");
    val = atof(str);
    printf("String value = %s, Float value = %f\n", str, val);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
   int val;
   char str[20];

   strcpy(str, "98993489");
   val = atoi(str);
   printf("String value = %s, Int value = %d\n", str, val);

   strcpy(str, "tutorialspoint.com");
   val = atoi(str);
   printf("String value = %s, Int value = %d\n", str, val);

   return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
    long val;
    char str[20];

    strcpy(str, "98993489");
    val = atol(str);
    printf("String value = %s, Long value = %ld\n", str, val);

    strcpy(str, "tutorialspoint.com");
    val = atol(str);
    printf("String value = %s, Long value = %ld\n", str, val);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    char str[30] = "20.30300 This is test";
    char *ptr;
    double ret;

    ret = strtod(str, &ptr);
    printf("The number(double) is %lf\n", ret);
    printf("String part is |%s|", ptr);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    char str[30] = "2030300 This is test";
    char *ptr;
    long ret;

    ret = strtol(str, &ptr, 10);
    printf("The number(unsigned long integer) is %ld\n", ret);
    printf("String part is |%s|", ptr);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    char str[30] = "2030300 This is test";
    char *ptr;
    long ret;

    ret = strtoul(str, &ptr, 10);
    printf("The number(unsigned long integer) is %lu\n", ret);
    printf("String part is |%s|", ptr);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    int i, n;
    int *a;

    printf("Number of elements to be entered:");
    scanf("%d",&n);

    a = (int*)calloc(n, sizeof(int));
    printf("Enter %d numbers:\n",n);
    for( i=0 ; i < n ; i++ ) {
        scanf("%d",&a[i]);
    }

    printf("The numbers entered are: ");
    for( i=0 ; i < n ; i++ ) {
        printf("%d ",a[i]);
    }
    free( a );

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
    char *str;*/

    /* Initial memory allocation */
    /*str = (char *) malloc(15);
    strcpy(str, "tutorialspoint");
    printf("String = %s,  Address = %u\n", str, str);*/

    /* Reallocating memory */
    /*str = (char *) realloc(str, 25);
    strcat(str, ".com");
    printf("String = %s,  Address = %u\n", str, str);*/

    /* Deallocate allocated memory */
    /*free(str);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    char *str;*/

    /* Initial memory allocation */
    /*str = (char *) malloc(15);
    strcpy(str, "tutorialspoint");
    printf("String = %s,  Address = %u\n", str, str);*/

    /* Reallocating memory */
    /*str = (char *) realloc(str, 25);
    strcat(str, ".com");
    printf("String = %s,  Address = %u\n", str, str);

    free(str);

    return(0);
}
*/

/*int main () {
    char *str;*/

    /* Initial memory allocation */
    /*str = (char *) malloc(15);
    strcpy(str, "tutorialspoint");
    printf("String = %s,  Address = %u\n", str, str);*/

    /* Reallocating memory */
    /*str = (char *) realloc(str, 25);
    strcat(str, ".com");
    printf("String = %s,  Address = %u\n", str, str);

    free(str);

    return(0);
}
*/

/*
int main () {
    FILE *fp;

    printf("Going to open nofile.txt\n");
    fp = fopen( "nofile.txt","r" );
    if(fp == NULL) {
        printf("Going to abort the program\n");
        abort();
    }
    printf("Going to close nofile.txt\n");
    fclose(fp);

    return(0);
}
*/

/*
void functionA () {
    printf("This is functionA\n");
}

int main () {*/
    /* register the termination function */
    /*atexit(functionA );

    printf("Starting  main program...\n");

    printf("Exiting main program...\n");

    return(0);
}
*/

/*
int main () {
    printf("Start of the program....\n");

    printf("Exiting the program....\n");
    exit(0);

    printf("End of the program....\n");

    return(0);
}
*/

/*
int main () {
    printf("PATH : %s\n", getenv("PATH"));
    printf("HOME : %s\n", getenv("HOME"));
    printf("ROOT : %s\n", getenv("ROOT"));

    return(0);
}
*/

/*
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
    char command[50];

    strcpy( command, "ls -l" );
    system(command);

    return(0);
}

int main () {
    char command[50];

    strcpy( command, "dir" );
    system(command);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>


int cmpfunc(const void * a, const void * b) {
    return ( *(int*)a - *(int*)b );
}

int values[] = { 5, 20, 29, 32, 63 };

int main () {
    int *item;
    int key = 32;*/

    /* using bsearch() to find value 32 in the array */
    /*item = (int*) bsearch (&key, values, 5, sizeof (int), cmpfunc);
    if( item != NULL ) {
        printf("Found item = %d\n", *item);
    } else {
        printf("Item = %d could not be found\n", *item);
    }

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int values[] = { 88, 56, 100, 2, 25 };

int cmpfunc (const void * a, const void * b) {
    return ( *(int*)a - *(int*)b );
}

int main () {
    int n;

    printf("Before sorting the list is: \n");
    for( n = 0 ; n < 5; n++ ) {
        printf("%d ", values[n]);
    }

    qsort(values, 5, sizeof(int), cmpfunc);

    printf("\nAfter sorting the list is: \n");
    for( n = 0 ; n < 5; n++ ) {
        printf("%d ", values[n]);
    }

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    int a, b;

    a = abs(5);
    printf("value of a = %d\n", a);

    b = abs(-10);
    printf("value of b = %d\n", b);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    div_t output;

    output = div(27, 4);
    printf("Quotient part of (27/ 4) = %d\n", output.quot);
    printf("Remainder part of (27/4) = %d\n", output.rem);

    output = div(27, 3);
    printf("Quotient part of (27/ 3) = %d\n", output.quot);
    printf("Remainder part of (27/3) = %d\n", output.rem);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    long int a,b;

    a = labs(65987L);
    printf("Value of a = %ld\n", a);

    b = labs(-1005090L);
    printf("Value of b = %ld\n", b);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

int main () {
    ldiv_t output;

    output = ldiv(100000L, 30000L);

    printf("Quotient = %ld\n", output.quot);

    printf("Remainder = %ld\n", output.rem);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main () {
    int i, n;
    time_t t;

    n = 5;*/

    /* Intializes random number generator */
    /*srand((unsigned) time(&t));*/

    /* Print 5 random numbers from 0 to 49 */
    /*for( i = 0 ; i < n ; i++ ) {
        printf("%d\n", rand() % 50);
    }

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
    int len;
    char *pmbnull  = NULL;
    char *pmb = (char *)malloc( MB_CUR_MAX );
    wchar_t *pwc = L"Hi";
    wchar_t *pwcs = (wchar_t *)malloc( sizeof( wchar_t ));

    printf("Converting to multibyte string\n");
    len = wcstombs( pmb, pwc, MB_CUR_MAX);
    printf("Characters converted %d\n", len);
    printf("Hex value of first multibyte character: %#.4x\n", pmb);

    len = mblen( pmb, MB_CUR_MAX );
    printf( "Length in bytes of multibyte character %x: %u\n", pmb, len );

    pmb = NULL;

    len = mblen( pmb, MB_CUR_MAX );
    printf( "Length in bytes of multibyte character %x: %u\n", pmb, len );

    return(0);
}
*/

/*
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main () {
    int len;
    char *pmbnull  = NULL;
    char *pmb = (char *)malloc( MB_CUR_MAX );
    wchar_t *pwc = L"Hi";
    wchar_t *pwcs = (wchar_t *)malloc( sizeof( wchar_t ));

    printf("Converting to multibyte string\n");
    len = wcstombs( pmb, pwc, MB_CUR_MAX);
    printf("Characters converted %d\n", len);
    printf("Hex value of first multibyte character: %#.4x\n", pmb);

    printf("Converting back to Wide-Character string\n");
    len = mbstowcs( pwcs, pmb, MB_CUR_MAX);
    printf("Characters converted %d\n", len);
    printf("Hex value of first wide character %#.4x\n\n", pwcs);

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
    char *str = "This is tutorialspoint.com";
    wchar_t mb[100];
    int len;

    len = mblen(NULL, MB_CUR_MAX);

    mbtowc(mb, str, len*strlen(str) );

    wprintf(L"%ls \n", mb );

    return(0);
}
*/

/*
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 50

int main () {
    size_t ret;
    char *MB = (char *)malloc( BUFFER_SIZE );
    wchar_t *WC = L"http://www.tutorialspoint.com";*/

    /* converting wide-character string */
    /*ret = wcstombs(MB, WC, BUFFER_SIZE);

    printf("Characters converted = %u\n", ret);
    printf("Multibyte character = %s\n\n", MB);

    return(0);
}
*/

#include <stdio.h>
#include <stdlib.h>

int main () {
    int i;
    wchar_t wc = L'a';
    char *pmbnull = NULL;
    char *pmb = (char *)malloc(sizeof( char ));

    printf("Converting wide character:\n");
    i = wctomb( pmb, wc );
    printf("Characters converted: %u\n", i);
    printf("Multibyte character: %.1s\n", pmb);

    printf("Trying to convert when target is NULL:\n");
    i = wctomb( pmbnull, wc );
    printf("Characters converted: %u\n", i);
    /* this will not print any value */
    printf("Multibyte character: %.1s\n", pmbnull);

    return(0);
}
